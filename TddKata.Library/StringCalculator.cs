﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace TddKata.Library
{
    public class StringCalculator
    {
        private const string DelimiterDefinitionText = "//";

        public int Add(string input)
        {
            if (input == "")
            {
                return 0;
            }

            string[] delimiters = GetDelimiters(input);
            string sanitizedInput = GetSanitizedInput(input);

            string[] inputArray = sanitizedInput.Split(delimiters, StringSplitOptions.None);
            IEnumerable<int> numbers = inputArray.Select(int.Parse);

            return AddNumbers(numbers);
        }

        private static int AddNumbers(IEnumerable<int> numbers)
        {
            var invalidNumbers = new List<int>();
            var result = 0;

            foreach (var number in numbers)
            {
                if (number < 0)
                {
                    invalidNumbers.Add(number);
                    continue;
                }
                if (number > 1000)
                {
                    continue;
                }
                result = result + number;
            }

            if (invalidNumbers.Any())
            {
                throw new NegativesNotAllowedException(string.Join(",", invalidNumbers.ToArray()));
            }

            return result;
        }

        private static string GetSanitizedInput(string input)
        {
            if (!input.StartsWith(DelimiterDefinitionText))
            {
                return input;
            }
            if (input.Contains("["))
            {
                var sanitizedInput = Regex.Replace(input, "//\\[(.*)\\]\n", "");
                return sanitizedInput;
            }

            return input.Substring(4);
        }

        private static string[] GetDelimiters(string input)
        {
            var basicDelimiterArray = new[] {",", "\n"};
            if (!input.StartsWith(DelimiterDefinitionText))
            {
                return basicDelimiterArray;
            }

            if (input.Contains("["))
            {
                MatchCollection matches = Regex.Matches(input, "\\[(.*?)\\]");
                var customDelimiters = matches.Cast<Match>().Select(m => m.Groups[1].Value);
                var delimiters = basicDelimiterArray.Concat(customDelimiters).ToArray();
                return delimiters;
            }

            return new[] {",", "\n", input[2].ToString(CultureInfo.InvariantCulture)};
        }
    }
}
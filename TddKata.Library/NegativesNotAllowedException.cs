﻿using System;

namespace TddKata.Library
{
    [Serializable]
    public class NegativesNotAllowedException : Exception
    {
        public NegativesNotAllowedException()
        {
        }

        public NegativesNotAllowedException(string message) : base(message)
        {
        }
    }
}
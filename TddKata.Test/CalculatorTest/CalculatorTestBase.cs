﻿using TddKata.Library;

namespace TddKata.Test.CalculatorTest
{
    public abstract class CalculatorTestBase
    {
        protected string Input;
        protected readonly StringCalculator Calculator;

        public CalculatorTestBase()
        {
            Calculator = new StringCalculator();
        }

        protected void TheInputIs(string input)
        {
            Input = input;
        }
    }
}
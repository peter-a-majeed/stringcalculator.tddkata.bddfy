﻿using FluentAssertions;
using NUnit.Framework;
using TddKata.Library;
using TestStack.BDDfy;

namespace TddKata.Test.CalculatorTest.AddTest
{
    [TestFixture]
    [Story(AsA = "As Alice",
        IWant = "I want to handle corner cases when adding numbers",
        SoThat = "So that I can help our customers understand the limitations of our software",
        Title = "We receive invalid inputs")]
    public class InvalidInputTest : AddTestBase
    {
        [Test]
        public void WePassInANegativeNumber()
        {
            NegativesNotAllowedException exception = null;
            this.Given(t => t.TheInputIs("1,2,-3,-4"), "Given we pass the calculator an string containing negatives")
                .When(t => t.WeAddTheNumbers(), "When we try to add the input")
                .Then(() => exception = AdditionAction.ShouldThrow<NegativesNotAllowedException>().And, "We should "
                    + "throw a NegativesNotAllowedException")
                .And(() => exception.Message.Should().Contain("-3,-4"), "The exception message should contain the "
                    + "invalid negative numbers")
                .BDDfy("Our calculator tries to add negative numbers");
        }

        [Test]
        public void WeIncludeANumberGreaterThan1000()
        {
            this.Given(t => t.TheInputIs("2,1001"), "Given we pass the calculator an number larger than 1,000")
                .When(t => t.WeAddTheNumbers(), "When we try to add the input")
                .Then(t => t.TheResultShouldBe(2), "Only the numbers less than 1,000 should be included")
                .BDDfy("Our calculator tries to add numbers larger than 1,000");
        }
    }
}
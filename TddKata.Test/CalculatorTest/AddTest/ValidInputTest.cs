﻿using FluentAssertions;
using NUnit.Framework;
using TestStack.BDDfy;

namespace TddKata.Test.CalculatorTest.AddTest
{
    [TestFixture]
    [Story(AsA = "As Alice",
        IWant = "I want to add numbers represented in a string",
        SoThat = "So that I can help our customers add their numbers",
        Title = "We receive valid inputs")]
    public class ValidInputTest : AddTestBase
    {
        [Test]
        public void WePassInZeroNumbers()
        {
            this.Given(t => t.TheInputIs(""), "Given we pass the calculator an empty string")
                .When(t => t.WeAddTheNumbers(), "When we try to add the input")
                .Then(t => t.TheResultShouldBe(0), "The result should be 0")
                .BDDfy("Our calculator adds an empty string");
        }

        [Test]
        public void WePassInOneNumber()
        {
            this.Given(t => t.TheInputIs("1"), "Given we pass the calculator a single number - let's say 1")
                .When(t => t.WeAddTheNumbers(), "When we try to add the input")
                .Then(t => t.TheResultShouldBe(1), "The result should be 1")
                .BDDfy("Our calculator adds a single number");
        }

        [Test]
        public void WePassInTwoNumbers()
        {
            this.Given(t => t.TheInputIs("1,2"), "Given we pass the calculator two numbers separated by a comma")
                .When(t => t.WeAddTheNumbers(), "When we try to add the input")
                .Then(t => t.TheResultShouldBe(3), "The calculator should add the numbers")
                .BDDfy("Our calculator adds two numbers");
        }

        [Test]
        public void WeAddAnArbitrarilyLongStringOfNumbers()
        {
            const string given = "Given we pass the calculator an arbitrarily long comma-delimited list of numbers";
            this.Given(t => t.TheInputIs("1,2,3,4,5,6"), given)
                .When(t => t.WeAddTheNumbers(), "When we try to add the input")
                .Then(t => t.TheResultShouldBe(21), "The calculator should add the numbers")
                .BDDfy("Our calculator adds more than two numbers");
        }

        [Test]
        public void WeDelimitTheNumbersWithANewLine()
        {
            const string given = "Given we delimit the numbers with a new line";
            this.Given(t => t.TheInputIs("1\n2\n3\n4\n5\n6"), given)
                .When(t => t.WeAddTheNumbers(), "When we try to add the input")
                .Then(t => t.TheResultShouldBe(21), "The calculator should add the numbers")
                .BDDfy("Our calculator accepts new line delimited numbers");
        }

        [Test]
        public void WeSupportCustomDelimiters()
        {
            const string given = "Given we delimit the numbers with any arbitrary character (a semicolon in this case)";
            this.Given(t => t.TheInputIs("//;\n1;2;3;4;5;6"), given)
                .When(t => t.WeAddTheNumbers(), "When we try to add the input")
                .Then(t => t.TheResultShouldBe(21), "The calculator should add the numbers")
                .BDDfy("Our calculator accepts any character as the delimiter");
        }

        [Test]
        public void WeSupportMultipleCharacterDelimiters()
        {
            const string given = "Given we delimit the numbers with a multi-character delimiter";
            this.Given(t => t.TheInputIs("//[!@#]\n1!@#2!@#3!@#4!@#5!@#6"), given)
                .When(t => t.WeAddTheNumbers(), "When we try to add the input")
                .Then(t => t.TheResultShouldBe(21), "The calculator should add the numbers")
                .BDDfy("Our calculator accepts multiple characters as the delimiter");
        }

        [Test]
        public void WeSupportMultipleCustomCharacterDelimiters()
        {
            const string given = "Given we delimit the numbers with multiple custom character delimiters";
            this.Given(t => t.TheInputIs("//[*][%]\n1*2%3"), given)
                .When(t => t.WeAddTheNumbers(), "When we try to add the input")
                .Then(t => t.TheResultShouldBe(6), "The calculator should add the numbers")
                .BDDfy("Our calculator accepts multiple custom character delimiters");
        }

        [Test]
        public void WeSupportMultipleCustomMultipleCharacterDelimiters()
        {
            const string given = "Given we delimit the numbers with multiple custom multiple character delimiters";
            this.Given(t => t.TheInputIs("//[***][%%%]\n1***2%%%3"), given)
                .When(t => t.WeAddTheNumbers(), "When we try to add the input")
                .Then(t => t.TheResultShouldBe(6), "The calculator should add the numbers")
                .BDDfy("Our calculator accepts multiple custom multiple character delimiters");
        }
    }
}
﻿using System;
using FluentAssertions;

namespace TddKata.Test.CalculatorTest.AddTest
{
    public abstract class AddTestBase : CalculatorTestBase
    {
        protected int AdditionResult;
        protected Action AdditionAction;

        protected void WeAddTheNumbers()
        {
            AdditionAction = () => AdditionResult = Calculator.Add(Input);
        }

        protected void TheResultShouldBe(int expectedValue)
        {
            AdditionAction
                .ShouldNotThrow();
            AdditionResult
                .Should()
                .Be(expectedValue);
        }
    }
}
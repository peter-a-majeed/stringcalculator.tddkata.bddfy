A demonstration of Roy Osherove's TDD kata using [TestStack's BDDfy BDD testing library](http://docs.teststack.net/bddfy/).

Its output resembles Jasmine's test output.

![Jasmine Test Runner](https://engagedevcamp.files.wordpress.com/2013/04/jasmine-spec-runner.png?w=640)

##[BDDfy Passing Report](http://jsbin.com/nibucoqavo/1/)

![BDDfy success](http://i.imgur.com/l2hFI3p.png)

##[BDDfy Failing Report](http://jsbin.com/pitonalime/1/)

![BDDfy failure](http://i.imgur.com/k2blWRD.png)